#define USE_FCNTL 1

#ifdef APACHE2
#  if APR_HAVE_SYS_SOCKET_H
#    include <sys/socket.h>
#    include <netinet/in.h> 
#    include <netdb.h>
#  endif
#  if APR_HAVE_UNISTD_H
#    include <unistd.h>
#  endif
#  if APR_HAVE_SYS_TYPES_H
#    include <sys/types.h>
#  endif
#endif

#if defined(NETWARE)
#  define READ_FLAG (S_IREAD)
#  define RW_FLAG   (S_IREAD|S_IWRITE)
#elif defined(WIN32)
#  define READ_FLAG (_S_IREAD)
#  define RW_FLAG   (_S_IREAD|_S_IWRITE)
#else
#  define READ_FLAG (S_IRUSR)
#  define RW_FLAG   (S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH)
#endif

#ifdef APACHE2
#  define AP_POOL      apr_pool_t
#  define AP_STRDUP    apr_pstrdup
#  define AP_STRCAT    apr_pstrcat
#  define AP_ALLOC     apr_pcalloc
#  define AP_SPRINTF   apr_psprintf
#  define AP_SNPRINTF  apr_snprintf
#  define AP_VSNPRINTF apr_vsnprintf

#  define AP_TABLE_T   apr_table_t
#  define AP_TABLEGET  apr_table_get
#  define AP_TABLESET  apr_table_set
#  define AP_TABLE_ELTS apr_table_elts
#  define AP_TABLE_ENTRY_T apr_table_entry_t
#  define AP_TABLE_MAKE    apr_table_make

#  define AP_ARRAY_HEADER_T apr_array_header_t
#  define AP_TOUPPER   apr_toupper

#  define AP_FILEOPEN  apr_file_open
#  define AP_FILEREAD  apr_file_read
#  define AP_FILEWRITE apr_file_write
#  define AP_FILECLOSE apr_file_close

#  define AP_PERM_T    apr_fileperms_t
#  define AP_MODE_T    int
#  define AP_SIZE_T    apr_size_t
#  define AP_FILE_T    apr_file_t*
#  define AP_STATUS    apr_status_t
#  define AP_SUCCESS   APR_SUCCESS
#  define AP_ARRAYPUSH apr_array_push
#  define AP_ARRAYMAKE apr_array_make

#  define AP_READ      APR_READ
#  define AP_WRITE     APR_WRITE
#  define AP_APPEND    APR_APPEND
#  define AP_CREATE    APR_CREATE

#  define AP_UREAD     APR_UREAD
#  define AP_UWRITE    APR_UWRITE
#  define AP_GREAD     APR_GREAD
#  define AP_WREAD     APR_WREAD

#  define PROXYREQ     (PROXYREQ_REVERSE)

#  define cmd_conf     void
#  define cmd_arg      const char

#  define ap_send_http_header(r)
#  define ap_hard_timeout(str,r)
#  define ap_reset_timeout(r)
#  define ap_kill_timeout(r)

#else /* APACHE2 */

#  ifdef USE_FCNTL
     static struct flock   lock_it;
     static struct flock unlock_it;
#endif

#  define AP_POOL      pool
#  define AP_STRDUP    ap_pstrdup
#  define AP_STRCAT    ap_pstrcat
#  define AP_ALLOC     ap_palloc
#  define AP_SPRINTF   ap_psprintf
#  define AP_SNPRINTF  ap_snprintf
#  define AP_VSNPRINTF ap_vsnprintf

#  define AP_TABLE_T   table
#  define AP_TABLEGET  ap_table_get
#  define AP_TABLESET  ap_table_set
#  define AP_TABLE_ELTS ap_table_elts
#  define AP_TABLE_ENTRY_T table_entry
#  define AP_TABLE_MAKE    ap_make_table

#  define AP_ARRAY_HEADER_T array_header
#  define AP_TOUPPER   ap_toupper

#  define AP_FILEOPEN  ap_popenf
#  define AP_FILEREAD  read
#  define AP_FILEWRITE write
#  define AP_FILECLOSE(fp) ap_pclosef(r->pool,fp)

#  define AP_PERM_T    int
#  define AP_MODE_T    mode_t
#  define AP_SIZE_T    size_t
#  define AP_FILE_T    int
#  define AP_STATUS    int
#  define AP_SUCCESS   0
#  define AP_ARRAYPUSH ap_push_array
#  define AP_ARRAYMAKE ap_make_array

#  define AP_READ      READ_FLAG
#  define AP_WRITE     O_WRONLY
#  define AP_APPEND    O_APPEND
#  define AP_CREATE    O_CREAT

#  define AP_UREAD     READ_FLAG
#  define AP_UWRITE    RW_FLAG
#  define AP_GREAD     RW_FLAG
#  define AP_WREAD     RW_FLAG

#  define PROXYREQ     PROXY_PASS

#  define cmd_conf    tequila_perdir_conf
#  define cmd_arg     char

   static void init_module (server_rec *s, AP_POOL *p);

#endif /* APACHE2 */

typedef struct {
  char   *key;
  char   *org;
  char   *user;
  char   *host;
  char   *path;
  char   *hash;
  char   *filter;
  AP_TABLE_T *attrs;
} Session;

typedef struct {
  char   *logfile;
  char   *teqserver;
  char   *teqserverurl;
  char   *sessdir;
  time_t sessmax;
  int    loglevel;
  int    usessl;
  char   *cafile;
  int    checkservername;
  int    securecookie;
  AP_FILE_T logfp;
} tequila_global_conf;

typedef struct {
  char *path;
  char *rewrite;
  char *resource;
  char *keyfile;
  char *certfile;
  char *identities;
  char *confirmuser;
  char *servname;
  char *authstrength;
  Session *session;
  AP_ARRAY_HEADER_T *customargs;
  AP_ARRAY_HEADER_T *allowif;
  AP_ARRAY_HEADER_T *allownet;
  AP_ARRAY_HEADER_T *allows;
  AP_ARRAY_HEADER_T *request;
} tequila_perdir_conf;

typedef struct filter {
  char *nam;
  char *val;
  int status;
  struct filter *next;
} Filter;

static int default_loglevel = 2;
static int proxy_available;

static const char *cmd_rewrite      (cmd_parms *cmd, cmd_conf *dconf, cmd_arg *path);
static const char *cmd_allowif      (cmd_parms *cmd, cmd_conf *dconf, cmd_arg *cond);
static const char *cmd_request      (cmd_parms *cmd, cmd_conf *dconf, cmd_arg *req);
static const char *cmd_allownet     (cmd_parms *cmd, cmd_conf *dconf, cmd_arg *net);
static const char *cmd_allows       (cmd_parms *cmd, cmd_conf *dconf, cmd_arg *cond);
static const char *cmd_service      (cmd_parms *cmd, cmd_conf *dconf, cmd_arg *serv);
static const char *cmd_authstrength (cmd_parms *cmd, cmd_conf *dconf, cmd_arg *authstrength);
static const char *cmd_resource     (cmd_parms *cmd, cmd_conf *vconf, cmd_arg *resname);
static const char *cmd_sslkeyfile   (cmd_parms *cmd, cmd_conf *vconf, cmd_arg *keyfile);
static const char *cmd_sslcertfile  (cmd_parms *cmd, cmd_conf *vconf, cmd_arg *certfile);
static const char *cmd_identities   (cmd_parms *cmd, cmd_conf *vconf, cmd_arg *identities);
static const char *cmd_confirmuser  (cmd_parms *cmd, cmd_conf *vconf, cmd_arg *confirmuser);
static const char *cmd_customarg    (cmd_parms *cmd, cmd_conf *vconf, cmd_arg *arg);

static const char *cmd_server    (cmd_parms *cmd, void *dummy, cmd_arg *server);
static const char *cmd_serverurl (cmd_parms *cmd, void *dummy, cmd_arg *serverurl);
static const char *cmd_sessdir   (cmd_parms *cmd, void *dummy, cmd_arg *sessdir);
static const char *cmd_sessmax   (cmd_parms *cmd, void *dummy, cmd_arg *sessmax);
static const char *cmd_log       (cmd_parms *cmd, void *dummy, cmd_arg *logfile);
static const char *cmd_loglevel  (cmd_parms *cmd, void *dummy, cmd_arg *loglevel);
static const char *cmd_nossl     (cmd_parms *cmd, void *dummy);
static const char *cmd_usessl    (cmd_parms *cmd, void *dummy);
static const char *cmd_cafile    (cmd_parms *cmd, void *dummy, cmd_arg *cafile);
static const char *cmd_checkservername (cmd_parms *cmd, void *dummy);
static const char *cmd_securecookie (cmd_parms *cmd, void *dummy);

static    int translate (request_rec *r);
static    int check_access (request_rec *r);
static   char *getkey (request_rec *r);
static   char *skip_match (char *uri, char *prefix);
static Session *readsession (request_rec *r, char *key);
static     int writesession (request_rec *r, char *sessdir, Session *session);
static   char *getFilter (request_rec *r);
static Filter *parsefilter (cmd_parms *cmd, const char *str);
static   char *getRequest (request_rec *r);
static   char *getAllows (request_rec *r);
static   void *create_perdir_config (AP_POOL *p, char *path);
static   void *merge_perdir_config  (AP_POOL *p, void *d1_conf, void *d2_conf);
static   void *create_global_config (AP_POOL *p, server_rec *s);
static   void *merge_global_config  (AP_POOL *p, void *serv1_conf, void *serv2_conf);
static   void openteqlog  (server_rec *s, AP_POOL *p);
static   void teqlog (request_rec *r, int level, const char *text, ...);
static   char *current_logtime (request_rec *r);
#ifndef APACHE2
static   void fd_lock (request_rec *r, int fd);
static   void fd_unlock(request_rec *r, int fd);
#endif /* APACHE2 */
static   void set_cookie (request_rec *r, char *key);
static   char *extractkeyfromcookie (request_rec *r, const char *cookie);
static unsigned hash (char *string);
static int check_net (request_rec *r);
static void register_hooks (AP_POOL *p);
static    int post_config  (AP_POOL *p, AP_POOL *plog, AP_POOL *ptemp, server_rec *s);

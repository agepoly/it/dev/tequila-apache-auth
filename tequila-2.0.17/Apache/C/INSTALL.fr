Pour installer le module apache de Tequila:

- t�l�charger la derni�re version de tequila-apache-C-[nnn].tgz
  depuis le serveur http://tequila.epfl.ch/download.html
- d�compresser le fichier tar
	tar zxf tequila-apache-C-[nnn].tgz
- aller dans le r�pertoire tequila-[nnn]/modules/C/
- v�rifier que le package httpd-devel est install�
- compiler :
	make
- installer (il faut ici �tre root, pour l'installation) :
	make install

- si vous avez apache1 :
	make -f Makefile.apache1 install

Remarques:

Le r�pertoire pas d�faut pour les sessions est /var/www/Tequila/Sessions.
Ce r�pertoire doit donc exister et �tre accessible en �criture � Apache.

Dans certaines distribution Linux (Debian), le r�pertoire
/var/www/Tequila/Sessions/ est accessible via le Web, dans ce cas, vous
devez IMPERATIVEMENT soit changer cet r�pertoire (directive TequilaSessionDir),
soit modifier votre configuration Apache pour le rendre invisible.

Si vous avez une version de tequila sous forme de rpm, la
supprimer avant d'installer cette version sinon la suppression
du rpm entraînera la suppresion du module install�
